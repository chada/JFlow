package bp.wf.template;

/** 
 节点方向属性
*/
public class DirectionAttr
{
	/** 
	 节点
	*/
	public static final String Node = "Node";
	/** 
	 转向的节点
	*/
	public static final String ToNode = "ToNode";
	/** 
	 流程编号
	*/
	public static final String FK_Flow = "FK_Flow";
	/** 
	 顺序
	*/
	public static final String Idx = "Idx";
}